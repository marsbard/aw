#!/bin/bash
#
# opinionated bootstrap
#
# expect env vars:
#    DOCK=blah # which dock to run, e.g. 'graylog'
#    RUN=/boo/far/bin/baz # what to run to stay alive

cd "`dirname $0`"
# we put this on during docker so make sure all up to date
SUM=`md5sum deploy.sh`
git pull
SUM1=`md5sum deploy.sh`
echo SUM=$SUM
echo SUM1=$SUM1
if [ "$SUM" != "$SUM1" ]
then
    echo deploy.sh changed since docker, re-run
    /opt/aw/deploy.sh
    exit
fi

date > /root/DEPLOY_START

# https://ask.puppetlabs.com/question/10955/issues-with-ubuntu-docker-image-and-puppetlabs-mysql/
#mv /sbin/initctl /sbin/oldinitctl
#echo -e '#!/bin/bash\nif [ $1 == "--version" ]\nthen\n  echo "initctl (upstart 1.12.1)"\nfi\n/sbin/oldinitctl "$@"' > /sbin/initctl
#chmod 755 /sbin/initctl

./install/puppet-bootstrap.sh
./install/puppetmods.sh

echo "class { 'aw': install_graylog => true, }" > go.pp
puppet apply --modulepath=puppet/modules go.pp || true

echo Now run $RUN
$RUN

# and if we get this far...

echo now keep running so we hold the docker open
while true
do
	sleep 60
	date
done
