#!/bin/bash
# 
# run from host

MODS="puppetlabs/vcsrepo elasticsearch/elasticsearch puppetlabs-apt graylog2/graylog2 puppetlabs/mongodb puppetlabs/stdlib Flameeyes/sysctl puppetlabs-concat nanliu/staging KyleAnderson/consul"

for mod in $MODS
do

	puppet module install --force --modulepath=puppet/modules $mod

done
