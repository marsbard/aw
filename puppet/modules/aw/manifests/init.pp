class aw (
  $install_graylog = false,
  $graylog_root_password_sha2 = "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918" # 'admin'
)
{

  if $install_graylog {
    class { 'aw::docks::graylog':
      server_password_secret => generate("/usr/bin/pwgen", "-s", "96", "1"),
      web_application_secret => generate("/usr/bin/pwgen", "-s", "96", "1"),
      server_root_password_sha2 => $graylog_root_password_sha2,
    }
  }


  define safe-download (
    $url,				# complete url to download the file from
    $filename,				# the filename of the download package
    $download_path,			# where to put the file
    $user = 'tomcat',
    $timeout = 0,
  ) { 
    exec { "safe-clean-any-old-${title}":
      command => "/bin/rm -f ${download_path}/tmp__${filename}",
      creates => "${download_path}/${filename}",
      require => File[$download_path],
      user => $user,
      timeout => $timeout,
    } ->  
    exec { "safe-retrieve-${title}":
      command => "/usr/bin/wget ${url} -O ${download_path}/tmp__${filename}",
      creates => "${download_path}/${filename}",
      user => $user,
      timeout => $timeout,
    } ->
    exec { "safe-move-${title}":
      command => "/bin/mv ${download_path}/tmp__${filename} ${download_path}/${filename}",
      creates => "${download_path}/${filename}",
      user => $user,
      timeout => $timeout,
    }   
  }



}
