class aw::docks::graylog (
  $server_password_secret,
  $server_root_password_sha2,
  $web_application_secret
)
{
#  include apt


#  class { 'sysctl':
#  } 
#
#  sysctl::value { 'net.ipv6.conf.all.disable_ipv6': content => '1' }
#  sysctl::value { 'net.ipv6.conf.default.disable_ipv6': content => '1' }
#  sysctl::value { 'net.ipv6.conf.lo.disable_ipv6': content => '1' }

#  file { "/etc/apt/apt.conf.d/99auth":       
#    owner     => root,
#    group     => root,
#    content   => "APT::Get::AllowUnauthenticated yes;",
#    mode      => 644;
#  } ->
  exec { 'get elasticsearch key':
    #command => 'wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -',
    command => 'rpm --import http://packages.elasticsearch.org/GPG-KEY-elasticsearch',
    path => '/usr/bin:/bin',
  } ->
  file { '/etc/yum.repos.d/elasticsearch.repo':
    ensure => present,
    content => "[elasticsearch-1.4]
name=Elasticsearch repository for 1.4.x packages
baseurl=http://packages.elasticsearch.org/elasticsearch/1.4/centos
gpgcheck=1
gpgkey=http://packages.elasticsearch.org/GPG-KEY-elasticsearch
enabled=1",
  } ->
#  exec { 'get elastic':
#    command => '/usr/bin/wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.1.deb',
#    cwd => '/tmp',
#  } ->
#  exec { 'install elastic':
#    command => '/usr/bin/dpkg -i elasticsearch-1.7.1.deb',
#    cwd => '/tmp',
#  } ->
#  exec { 'apt-get update':
#    command => '/usr/bin/apt-get update'
#  } ->
#  package { 'default-jdk': 
  package { 'java-1.8.0-openjdk':
    ensure => installed,
  } ->
  package { 'elasticsearch': 
    ensure => installed,
  } ->
  file { '/etc/elasticsearch/elasticsearch.yml':
    source => 'puppet:///modules/aw/graylog/elasticsearch.yml',
    ensure => present,
  } ->
  class { 'mongodb':
  } ->
  class {'graylog2::repo':
    version => '1.1'
  } ->
  class {'graylog2::server':
    password_secret    => $server_password_secret,
    root_password_sha2 => $server_root_password_sha2,
  } ->
  class {'graylog2::web':
    application_secret => $web_application_secret,
  } ->
  exec  {'get-graylog-collector':
    #command => 'wget https://packages.graylog2.org/repo/packages/graylog-collector-latest-repository-ubuntu14.04_latest.deb',
    command => 'rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-collector-latest-repository-el7_latest.rpm',
    path => '/usr/bin',
    cwd => '/tmp',
  } ->
  exec { 'install-graylog-collector':
    #command => 'dpkg -i graylog-collector-latest-repository-ubuntu14.04_latest.deb',
    command => 'yum install graylog-collector',
    path => '/usr/bin:/bin:/sbin:/usr/sbin',
    cwd => '/tmp',
  } ->
  #exec {'/usr/bin/apt-get update -y': } ->
  exec { '/usr/bin/yum update -y': } ->
  package { 'graylog-collector': 
    ensure => installed,
  } ->
  file { '/etc/graylog/collector/collector.conf':
    source => 'puppet:///modules/aw/graylog/collector.conf',
    ensure => present,
  } ~>
  service { 'graylog-collector':
    ensure => running,
  } ->
  service { 'elasticsearch':
    ensure => running,
  } ->
  file {'/tmp/setup-input.sh':
    source => 'puppet:///modules/aw/graylog/setup-input.sh',
    mode => '0755',
    ensure => present,
  } ->
  exec { '/tmp/setup-input.sh':
  } 

}

