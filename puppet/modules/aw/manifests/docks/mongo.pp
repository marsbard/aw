class aw::docks::mongo { 


  # sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
  include apt
  apt::key { 'mongo':
    id => '7F0CEB10', # short key triggers security warning
    server => 'keyserver.ubuntu.com',
  } ->
  # echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
  apt::source { 'mongo':
    location => 'http://repo.mongodb.org/apt/debian',
    release => 'wheezy/mongodb-org/3.0',
    repos => 'main',
    key => {
      'id' => '7F0CEB10',
      'server' => 'keyserver.ubuntu.com',
    },
    include => { 'deb' => true, },
  } ->
  package { 'mongodb-org':
    ensure => present,
  } ->
  file { '/etc/supervisor/conf.d/mongo.sv.conf':
    source => 'puppet:///aw/mongo/mongo.sv.conf',
    ensure => present,
  } ->
  service { 'mongod':
    ensure => running,
  }
}

