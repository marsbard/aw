class aw::docks::graylog (
  $server_password_secret,
  $server_root_password_sha2,
  $web_application_secret
)
{

  class { '::consul':
    config_hash => {
      data_dir   => '/opt/consul',
      log_level  => 'INFO',
      node_name  => 'db_log',
      retry_join => ['172.17.0.1'],
    } ,
	 	manage_service => false,
		#service_ensure => running,
		#service_enable => true,
	} ~> 
	exec { '/etc/init.d/consul restart':
	  path => '/bin:/usr/bin:/sbin:/usr/sbin',
	}


  $graylog_deb_name = 'graylog-1.2-repository-ubuntu14.04_latest.deb'
  $graylog_deb_url = "https://packages.graylog2.org/repo/packages/$graylog_deb_name"

  file { '/opt/download':
    ensure => directory,
  }

  exec { 'get graylog deb':
    command => "/usr/bin/wget $graylog_deb_url",
    require => File['/opt/download'],
    cwd => '/opt/download',
		unless => "/usr/bin/test -f /opt/download/$graylog_deb_name",
  } ->
  exec { 'install graylog deb':
    command => '/usr/bin/dpkg -i /opt/download/graylog*.deb',
		unless => '/usr/bin/test -f /usr/share/doc/graylog-1.2-repository-ubuntu14.04/changelog.Debian.gz',
  } ->
  exec { '/usr/bin/apt-get -y install apt-transport-https': } ->
  exec { '/usr/bin/apt-get update': } ->
  exec { '/usr/bin/apt-get -y install graylog-server graylog-web': timeout => 0 } ->
  service { 'graylog-server': ensure => running, } ->
  service { 'graylog-web': ensure => running, }


}
