class aw::docks::elastic { 

  $elastic_deb_url = 'https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.3.deb'


  exec {'get elastic deb':
    command => "/usr/bin/wget $elastic_deb_url",
    cwd => '/tmp',
  } ->
  exec { '/usr/bin/dpkg -i /tmp/elasticsearch*.deb': } ->
  service { 'elasticsearch': ensure => running, }


}

