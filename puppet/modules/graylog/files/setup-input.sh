WGET_TIMEOUT=20

function check_httpserv_up {
  NAME=$1
  URL=$2
  echo check_httpserv_up: $NAME: $URL
  RES=`wget -O /dev/null -T ${WGET_TIMEOUT} --no-check-certificate --server-response $URL 2>&1 | awk '/^  HTTP/{print $2}' | tail -n 1`
  if [ "$RES" == "" ]
    then
    echo check_httpserv_up: No response from server
    return 1
  fi
  if [ "$RES" -ge 300 ]
    then
    echo check_httpserv_up: ERROR: $RES response from server
    return 2
  fi
  echo check_httpserv_up: SUCCESS: $RES response from server
  return 0
}

COUNT=0
while true
do
	check_httpserv_up "graylog" "http://graylog-1:12900/system/lbstatus"
	if [ $? = 0 ]
	then
		break
	fi
	COUNT=$(( $COUNT + 1 ))
	if [ $COUNT -ge 10 ]
	then
		echo "Exceeded max tries for waiting for server"
		exit 99
	fi
	sleep 10
done

curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" --data '{
  "title": "collector input",
  "type": "org.graylog2.inputs.gelf.tcp.GELFTCPInput",
  "configuration": {
    "port": 12201,
    "bind_address": "0.0.0.0"
  }
}' http://admin:admin@graylog-1:12900/system/inputs

exit $?
